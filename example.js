'use strict';

// Runtime variables, used to track the current program state:
var example_e = null;  // The HTML element used for input/output.
var example_c = 0;     // Stores the current "cursor" position.
var example_i = 0;     // Stores the current "jump to" position.

// BelchScript variables, as per the specification:
var example_ARGY = false;
var example_HURL = 0;
var example_MUCK = 0;

// This function runs the script. Users should provide a string for 'args'.
function example(args)
{

  var key_s = '';

  // This function safely updates the cursor position so that it wraps around.
  var incc = function(delta)
  {
    if (example_e.value.length < 2) { example_c = 0; return; }
    example_c += delta;
    while (example_c >= example_e.value.length) { example_c -= example_e.value.length; }
    while (example_c <  0                   ) { example_c += example_e.value.length; }
  }

  // This is used in various places to update the display text.
  var puts = function(txt)
  {
    txt = String(txt); if (txt.length < 1) { return; }
    example_e.value = example_e.value.slice(0, example_c) + txt + example_e.value.slice(example_c + txt.length, example_e.value.length);
    incc(txt.length - 1);
  }

  // Get the code value of the character at the current cursor position.
  var getv = function() { return example_e.value.charCodeAt(example_c); }

  // This function is called either (A) to set things up or (B) as a callback.
  if (typeof(args) === 'string')
  {

    // If the function WASN'T called in response to an event, initialise stuff:
    example_c = 0;
    example_i = 0;

    example_ARGY = false;
    example_HURL = 0;
    example_MUCK = 0;

    // Create an element to store the text buffer and receive input events.
    example_e = document.createElement('input');
    example_e.setAttribute('id', 'example');
    example_e.setAttribute('readonly', 'readonly');
    example_e.onkeypress = example;
    puts(args);

    // Insert the new element into the HTML document.
    var old = document.getElementById('example');
    if (old) { old.parentNode.replaceChild(example_e, old); }
    else     { document.body.appendChild(example_e);        }

  }
  else
  {

    // If this is a callback based on a key press, get the value as a string.
    if      (args.char    ) { key_s = args.char;                          }
    else if (args.charCode) { key_s = String.fromCharCode(args.charCode); }
    else                    { return;                                     }

  }

  // Emulate GOTO-like behaviour for jump instructions.
  while (true) switch (example_i)
  {
    default:
    /* Arse */ puts('Hello World! ');
    /* Chukk */ example_MUCK = getv();
    /* Arse */ puts("12345678901234567890123456 ");
    /* Plop */ incc(-getv());
    /* EGGZ */ case 26:
    /* Cruel */ example_i = 29; return; case 29: puts(key_s);
    /* Churl */ example_HURL = getv();
    /* Whizzz */ example_ARGY = example_HURL == example_MUCK;
    /* Ham */ if (!example_ARGY) { example_i = 26; continue; };
    /* Arse */ puts("Done");
    /* Snot */ example_e.onkeypress = function(e) {}; return;
    example_i = 0;  // Go back to the start of the program again.
  }
}
