'use strict';

// Runtime variables, used to track the current program state:
var test_e = null;  // The HTML element used for input/output.
var test_c = 0;     // Stores the current "cursor" position.
var test_i = 0;     // Stores the current "jump to" position.

// BelchScript variables, as per the specification:
var test_ARGY = false;
var test_HURL = 0;
var test_MUCK = 0;

// This function runs the script. Users should provide a string for 'args'.
function test(args)
{

  var key_s = '';

  // This function safely updates the cursor position so that it wraps around.
  var incc = function(delta)
  {
    if (test_e.value.length < 2) { test_c = 0; return; }
    test_c += delta;
    while (test_c >= test_e.value.length) { test_c -= test_e.value.length; }
    while (test_c <  0                   ) { test_c += test_e.value.length; }
  }

  // This is used in various places to update the display text.
  var puts = function(txt)
  {
    txt = String(txt); if (txt.length < 1) { return; }
    test_e.value = test_e.value.slice(0, test_c) + txt + test_e.value.slice(test_c + txt.length, test_e.value.length);
    incc(txt.length - 1);
  }

  // Get the code value of the character at the current cursor position.
  var getv = function() { return test_e.value.charCodeAt(test_c); }

  // This function is called either (A) to set things up or (B) as a callback.
  if (typeof(args) === 'string')
  {

    // If the function WASN'T called in response to an event, initialise stuff:
    test_c = 0;
    test_i = 0;

    test_ARGY = false;
    test_HURL = 0;
    test_MUCK = 0;

    // Create an element to store the text buffer and receive input events.
    test_e = document.createElement('input');
    test_e.setAttribute('id', 'test');
    test_e.setAttribute('readonly', 'readonly');
    test_e.onkeypress = test;
    puts(args);

    // Insert the new element into the HTML document.
    var old = document.getElementById('test');
    if (old) { old.parentNode.replaceChild(test_e, old); }
    else     { document.body.appendChild(test_e);        }

  }
  else
  {

    // If this is a callback based on a key press, get the value as a string.
    if      (args.char    ) { key_s = args.char;                          }
    else if (args.charCode) { key_s = String.fromCharCode(args.charCode); }
    else                    { return;                                     }

  }

  // Emulate GOTO-like behaviour for jump instructions.
  while (true) switch (test_i)
  {
    default:
    /* ARSE */ puts("Hello ");
    /* Cruel */ test_i = 3; return; case 3: puts(key_s);
    /* ARSE */ puts("World!");
    /* SNOT */ test_e.onkeypress = function(e) {}; return;
    test_i = 0;  // Go back to the start of the program again.
  }
}
