belch2js
========

`belch2js` [transcompiles](https://en.wikipedia.org/wiki/Transpiler) BelchScript
source code into Javascript so that it can be executed in any modern browser. It
also supports literate programming via `.litbelch` files, an idea borrowed from
[CoffeeScript](http://coffeescript.org/).

About BelchScript
-----------------

The original BelchScript specification was created sometime around 2007 by
[Edward Biddulph](https://twitter.com/eddbiddulph), and is included with the
`belch2js` source code - see `belch.txt` for details. `belch2js` differs from
the orignal specification slightly:

+ Newlines are used to separate each statement instead of semicolons.
+ Lines starting with a # character are treated as comments, as per Python/Ruby.

The original specification was ambiguous about case sensitivity, so `belch2js`
allows UPPER, lower, or MiXeD cAsE instructions to be used.

Using BelchScript
-----------------

First, convert your `abcde.belch` file(s) into Javascript using `belch2js`:

	thomas@debian-laptop:~/Programming/belchscript$ ./belch2js.py abcde.belch
	abcde.belch: Success!
	Finished.
	thomas@debian-laptop:~/Programming/belchscript$ 

*NOTE: You may have to type* `python ./belch2js.py abcde.belch` *instead.*

Now you simply need to include the `abcde.js` file in your HTML document and run
the `abcde()` function it provides, specifying the initial text as a parameter:

	<!DOCTYPE html>
	<html>
		<head>
		    <meta charset="utf-8">
		    <title>My BelchScript Page.</title>
		</head>
		<body onload="abcde('')">
		    <script src="abcde.js" type="text/javascript"></script>
		</body>
	</html>

Note that a placeholder element can be used to insert the BelchScript text field
at a specific location - simply set the `id` attribute to match the name of the
script, and it will be replaced when the program is executed for the first time.
If no such placeholder element exists, the BelchScript output is simply added to
the bottom of the `body` tag.

It's also possible to embed multiple BelchScript items within a single HTML page
as long as they all have different names, so `myexample1` and `myexample2` could
both be run concurrently within the same document.

It's possible that other "output" languages (such as Python, Ruby, or C) will be
supported in the future but for now only Javascript code generation is possible.

License
-------

Copyright (c) 2015 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
