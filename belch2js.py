#!/usr/bin/python
"""
belch2js - Transcompiles a .belch or .litbelch file into standard Javascript.

Copyright (c) 2015 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

# =============================================================================

JS_PREFIX = """'use strict';

// Runtime variables, used to track the current program state:
var belch_e = null;  // The HTML element used for input/output.
var belch_c = 0;     // Stores the current "cursor" position.
var belch_i = 0;     // Stores the current "jump to" position.

// BelchScript variables, as per the specification:
var belch_ARGY = false;
var belch_HURL = 0;
var belch_MUCK = 0;

// This function runs the script. Users should provide a string for 'args'.
function belch(args)
{

  var key_s = '';

  // This function safely updates the cursor position so that it wraps around.
  var incc = function(delta)
  {
    if (belch_e.value.length < 2) { belch_c = 0; return; }
    belch_c += delta;
    while (belch_c >= belch_e.value.length) { belch_c -= belch_e.value.length; }
    while (belch_c <  0                   ) { belch_c += belch_e.value.length; }
  }

  // This is used in various places to update the display text.
  var puts = function(txt)
  {
    txt = String(txt); if (txt.length < 1) { return; }
    belch_e.value = belch_e.value.slice(0, belch_c) + txt + belch_e.value.slice(belch_c + txt.length, belch_e.value.length);
    incc(txt.length - 1);
  }

  // Get the code value of the character at the current cursor position.
  var getv = function() { return belch_e.value.charCodeAt(belch_c); }

  // This function is called either (A) to set things up or (B) as a callback.
  if (typeof(args) === 'string')
  {

    // If the function WASN'T called in response to an event, initialise stuff:
    belch_c = 0;
    belch_i = 0;

    belch_ARGY = false;
    belch_HURL = 0;
    belch_MUCK = 0;

    // Create an element to store the text buffer and receive input events.
    belch_e = document.createElement('input');
    belch_e.setAttribute('id', 'belch');
    belch_e.setAttribute('readonly', 'readonly');
    belch_e.onkeypress = belch;
    puts(args);

    // Insert the new element into the HTML document.
    var old = document.getElementById('belch');
    if (old) { old.parentNode.replaceChild(belch_e, old); }
    else     { document.body.appendChild(belch_e);        }

  }
  else
  {

    // If this is a callback based on a key press, get the value as a string.
    if      (args.char    ) { key_s = args.char;                          }
    else if (args.charCode) { key_s = String.fromCharCode(args.charCode); }
    else                    { return;                                     }

  }

  // Emulate GOTO-like behaviour for jump instructions.
  while (true) switch (belch_i)
  {
    default:
"""

# =============================================================================

JS_POSTFIX = """    belch_i = 0;  // Go back to the start of the program again.
  }
}
"""

# =============================================================================

import os


def main(file_name):
    """Transcompiles a .belch or .litbelch file into standard Javascript."""
    script_path, script_type = os.path.splitext(file_name)
    script_name = os.path.basename(script_path)
    # Open both the original BelchScript file and the output Javascript file.
    try:
        with open(file_name, "r") as src_file:
            with open(script_path + ".js", "w") as dst_file:
                # Assume that the first EGGZ is the program start point.
                last_eggs = "0"
                # Output some boilerplate Javascript code to get things ready.
                dst_file.write(JS_PREFIX.replace("belch", script_name))
                # Loop through the lines of source code in the input file.
                for line_num, line in enumerate(src_file.readlines()):

                    # Literate Belchscript works as per literate Coffeescript:
                    if script_type.strip().lower() == '.litbelch':
                        if not line.startswith(('    ', '\t')):
                            continue  # Ignore any non-code lines in Markdown.

                    # Ignore blank/comment lines.
                    line = line.strip()
                    if not line or line.startswith('#'):
                        continue

                    # Indent + Add a comment to show the original instruction.
                    dst_file.write("    /* " + line.split()[0].strip() + " */ ")

                    # ARSE is special: It's the only command with a parameter.
                    if line.upper().startswith('ARSE '):
                        # FIXME: Do this properly; JS injection is possible!
                        dst_file.write('puts(' + line[5:] + ');\n')
                        continue  # Move on to the next line of source code.

                    # All other commands can safely be handled as UPPER CASE.
                    line = line.upper()
                    if line == 'PLOP':
                        # Move the cursor LEFT by the value under the cursor.
                        dst_file.write('incc(-getv());\n')
                    elif line == 'SLOP':
                        # Move the cursor RIGHT by the value under the cursor.
                        dst_file.write('incc(+getv());\n')
                    elif line == 'WHIZZZ':
                        # Set ARGY to true if HURL == MUCK
                        dst_file.write(script_name + '_ARGY = ' + script_name + '_HURL == ' + script_name + '_MUCK;\n')
                    elif line == 'GRIME':
                        # Set ARGY to false if HURL == MUCK
                        dst_file.write(script_name + '_ARGY = ' + script_name + '_HURL != ' + script_name + '_MUCK;\n')
                    elif line == 'CHURL':
                        # Get the char under the cursor and put it in HURL.
                        dst_file.write(script_name + '_HURL = getv();\n')
                    elif line == 'CHUKK':
                        # Get the char under the cursor and put it in MUCK.
                        dst_file.write(script_name + '_MUCK = getv();\n')
                    elif line == 'BURRP':
                        # Set the char to MUCK if ARGY is true, otherwise HURL.
                        dst_file.write('puts(String.fromCharCode(' + script_name + '_ARGY ? ' + script_name + '_MUCK : ' + script_name + '_HURL));\n')
                    elif line == 'EGGZ':
                        # Define a new Loop Start point.
                        dst_file.write('case ' + str(line_num) + ':\n')
                        last_eggs = str(line_num)
                    elif line == 'HAM':
                        # Jump to the most recent EGGZ if ARGY is false.
                        dst_file.write('if (!' + script_name + '_ARGY) { ' + script_name + '_i = ' + last_eggs + '; continue; };\n')
                    elif line == 'BACON':
                        # Jump to the most recent EGGZ and decrement MUCK.
                        dst_file.write(script_name + '_MUCK -= 1; ' + script_name + '_i = ' + last_eggs + '; break;\n')
                    elif line == 'CRUEL':
                        # Get a char from the user and add it at the cursor.
                        dst_file.write(script_name + '_i = ' + str(line_num) + '; return; case ' + str(line_num) + ': puts(key_s);\n')
                    elif line == 'SNOT':
                        # Terminate the progam.
                        dst_file.write(script_name + '_e.onkeypress = function(e) {}; return;\n')
                    else:
                        # Anything else is an error.
                        return "[ERROR] Unknown line: " + line
                # Output the rest of the boilerplate Javascript code.
                dst_file.write(JS_POSTFIX.replace("belch", script_name))
    # If anything goes wrong when reading/writing the files, return the error.
    except EnvironmentError as e:
        return str(e)


# Loop through the command line parameters and convert each filename provided.
if __name__ == "__main__":
    import sys
    for _item in sys.argv[1:]:
        _error = main(_item)
        print(_item + ": " + (_error if _error else "Success!"))
    print("Finished.")
